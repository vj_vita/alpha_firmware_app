/******************************************************************************

 @file       vita_gatt_profile.h

 @brief This file contains the Vita GATT profile definitions and prototypes
        prototypes.

 Group: CMCU, SCS
 Target Device: CC2640R2

 ******************************************************************************
 
 Copyright (c) 2010-2017, Texas Instruments Incorporated
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 *  Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

 *  Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

 *  Neither the name of Texas Instruments Incorporated nor the names of
    its contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ******************************************************************************
 Release Name: simplelink_cc2640r2_sdk_01_50_00_58
 Release Date: 2017-10-17 18:09:51
 *****************************************************************************/

#ifndef VITAGATTPROFILE_H
#define VITAGATTPROFILE_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

/*********************************************************************
 * CONSTANTS
 */

// Profile Parameters
#define VITAPROFILE_DATA                   0  // RW uint8 - Profile Characteristic 1 value
#define VITAPROFILE_CONF                   1  // RW uint8 - Profile Characteristic 2 value
#define VITAPROFILE_DEBUG                  2  // RW uint8 - Profile Characteristic 3 value
#define VITAPROFILE_DEBUG_CONF             3  // RW uint8 - Profile Characteristic 4 value
#define VITAPROFILE_SPI_RESP               4  // RW uint8 - Profile Characteristic 5 value
#define VITAPROFILE_SPI_REQ                5  // RW uint8 - Profile Characteristic 6 value
#define VITAPROFILE_NIR_DATA               6  // RW uint8 - Profile Characteristic 7 value
#define VITAPROFILE_NIR_CONF               7  // RW uint8 - Profile Characteristic 8 value

// Simple Profile Service UUID
#define VITAPROFILE_SERV_UUID               0xAA70

// Service UUIDs
#ifdef VITA_BIA_SCAN
#define VITA_SERV_UUID              0xAA70
#define VITA_DATA_UUID              0xAA71
#endif
#define VITA_CONF_UUID              0xAA72
#define VITA_DEBUG_UUID             0xAA73
#define VITA_DEBUG_CONF_UUID        0xAA74
#define VITA_SPI_REQ_UUID           0xAA75
#define VITA_SPI_RESP_UUID          0xAA76
#ifdef VITA_NIR_SCAN
#define VITA_NIR_DATA_UUID          0xAA78
#define VITA_NIR_CONF_UUID          0xAA77
#endif

#ifdef VITA_BIA_SCAN
// Length of sensor data in bytes
#define VITA_DATA_LEN               20
// Length of sensor conf in bytes
#define VITA_CONF_LEN               20
#endif
// Length of sensor debug in bytes
#define VITA_DEBUG_LEN              20
// Length of sensor debug configuration in bytes
#define VITA_DEBUG_CONF_LEN         1
// Length of sensor SPI request in bytes
#define VITA_SPI_REQ_LEN            1
// Length of sensor SPI response in bytes
#define VITA_SPI_RESP_LEN           1
#ifdef VITA_NIR_SCAN
// Length of NIR sensor data in bytes
#define VITA_NIR_DATA_LEN           20
// Length of NIR sensor conf in bytes
#define VITA_NIR_CONF_LEN           20
#endif

// Simple Keys Profile Services bit fields
#define VITAPROFILE_SERVICE               0x00000001

//DEBUGGING
//FUNCTIONS
#define VITA_DEBUGGING_FN_Vita_init                                   0x0001
#define VITA_DEBUGGING_FN_Vita_processCharValueChangeEvt              0x0002

#define VITA_DEBUGGING_ENTRY       0x01
#define VITA_DEBUGGING_POLL        0x02
#define VITA_DEBUGGING_BIA_SCAN    0x03
#define VITA_DEBUGGING_NIR_SCAN    0x04
#define VITA_DEBUGGING_ECG_SCAN    0x05
#define VITA_DEBUGGING_SPI_REQ     0x06
#define VITA_DEBUGGING_LED_PIN     0x07
#define VITA_DEBUGGING_PWR_PIN     0x08
#define VITA_DEBUGGING_PWM_PIN     0x09
#define VITA_DEBUGGING_EXIT        0xFF

#define VITA_DEBUGGING_SUCCESS       0x01
#define VITA_DEBUGGING_FAILURE       0xFF

//OFFSETS
#define VITA_DEBUGGING_OFFSET_FN 0
#define VITA_DEBUGGING_OFFSET_OPERATION 2
#define VITA_DEBUGGING_OFFSET_SUCCESSFAILURE 3
#define VITA_DEBUGGING_OFFSET_DATA 4

/*********************************************************************
 * TYPEDEFS
 */


/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * Profile Callbacks
 */

// Callback when a characteristic value has changed
typedef void (*VitaProfileChange_t)( uint8 paramID );

typedef struct
{
    VitaProfileChange_t        pfnVitaProfileChange;  // Called when characteristic value changes
} VitaProfileCBs_t;



/*********************************************************************
 * API FUNCTIONS
 */


/*
 * VitaProfile_AddService- Initializes the Vita GATT Profile service by registering
 *          GATT attributes with the GATT server.
 *
 * @param   services - services to add. This is a bit map and can
 *                     contain more than one service.
 */

extern bStatus_t VitaProfile_AddService( uint32 services );

/*
 * VitaProfile_RegisterAppCBs - Registers the application callback function.
 *                    Only call this function once.
 *
 *    appCallbacks - pointer to application callbacks.
 */
extern bStatus_t VitaProfile_RegisterAppCBs( VitaProfileCBs_t *appCallbacks );

/*
 * VitaProfile_SetParameter - Set a Simple GATT Profile parameter.
 *
 *    param - Profile parameter ID
 *    len - length of data to right
 *    value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16 will be cast to
 *          uint16 pointer).
 */
extern bStatus_t VitaProfile_SetParameter( uint8 param, uint8 len, void *value );

/*
 * VitaProfile_GetParameter - Get a Simple GATT Profile parameter.
 *
 *    param - Profile parameter ID
 *    value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16 will be cast to
 *          uint16 pointer).
 */
extern bStatus_t VitaProfile_GetParameter( uint8 param, void *value );


/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* VITAGATTPROFILE_H */
